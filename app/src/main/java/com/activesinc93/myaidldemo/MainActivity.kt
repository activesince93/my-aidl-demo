package com.activesinc93.myaidldemo

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import androidx.appcompat.app.AppCompatActivity
import com.activesinc93.mylibrary.SensorData
import com.activesinc93.mylibrary.SensorDataService
import kotlinx.android.synthetic.main.activity_main.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class MainActivity : AppCompatActivity() {

    private lateinit var context: Context
    private var iSensorService: ISensorProvider? = null
    private var connection: SensorDataServiceConnection? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        context = this

        initViews()
        initService()
    }

    private fun initViews() {
        btnUpdateData.setOnClickListener {
            iSensorService?.getOrientationSensorData()
        }
    }

    private fun initService() {
        connection = SensorDataServiceConnection()

        val intent = Intent()
        intent.setClassName(packageName, SensorDataService::class.java.name)
        val ret = bindService(intent, connection!!, Context.BIND_AUTO_CREATE)
    }

    private fun releaseService() {
        unbindService(connection!!)
        connection = null
    }

    override fun onDestroy() {
        super.onDestroy()
        releaseService()
    }

    inner class SensorDataServiceConnection : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            iSensorService = ISensorProvider.Stub.asInterface(service)
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            iSensorService = null
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(sensor: SensorData) {
        val sensorStr = StringBuilder()
            .append("Pinch: ${sensor.pitch}")
            .append("\n")
            .append("Roll: ${sensor.roll}")
            .toString()
        tvSensorData.text = sensorStr
    }
}