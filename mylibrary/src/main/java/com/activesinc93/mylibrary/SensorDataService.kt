package com.activesinc93.mylibrary

import android.app.Service
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.IBinder
import com.activesinc93.myaidldemo.ISensorProvider
import org.greenrobot.eventbus.EventBus


/**
 * Created by Darshan Parikh on 18/10/20.
 */

class SensorDataService : Service() {

    private lateinit var rotationSensor: Sensor

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    private val binder = object: ISensorProvider.Stub() {
        override fun addNumbers(num1: Int, num2: Int): Int {
            return num1 + num2
        }

        override fun getOrientationSensorData() {
            val sensorManager = applicationContext.getSystemService(SENSOR_SERVICE) as SensorManager
            rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)

            sensorManager.registerListener(
                    object : SensorEventListener {
                        override fun onSensorChanged(event: SensorEvent?) {
                            if (event == null) return

                            val sensor = event.sensor ?: return
                            if (sensor != rotationSensor) return

                            if (event.values.size > 4) {
                                val truncatedRotationVector = FloatArray(4)
                                System.arraycopy(event.values, 0, truncatedRotationVector, 0, 4)
                                update(truncatedRotationVector)
                            } else {
                                update(event.values)
                            }
                        }

                        override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}
                    },
                    rotationSensor,
                    SENSOR_DELAY
            )
        }
    }

    private fun update(vectors: FloatArray) {
        val rotationMatrix = FloatArray(9)
        SensorManager.getRotationMatrixFromVector(rotationMatrix, vectors)

        val worldAxisX = SensorManager.AXIS_X
        val worldAxisZ = SensorManager.AXIS_Z
        val adjustedRotationMatrix = FloatArray(9)
        SensorManager.remapCoordinateSystem(rotationMatrix, worldAxisX, worldAxisZ, adjustedRotationMatrix)

        val orientation = FloatArray(3)
        SensorManager.getOrientation(adjustedRotationMatrix, orientation)

        val pitch: Float = orientation[1] * FROM_RADS_TO_DEGS
        val roll: Float = orientation[2] * FROM_RADS_TO_DEGS
        EventBus.getDefault().post(SensorData(pitch, roll))
    }

    companion object {
        private const val FROM_RADS_TO_DEGS = -57
        private const val SENSOR_DELAY = 8 * 1000 // 8ms
    }
}