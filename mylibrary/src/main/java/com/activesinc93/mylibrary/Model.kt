package com.activesinc93.mylibrary

/**
 * Created by Darshan Parikh on 19/10/20.
 */

data class SensorData(
    val pitch: Float,
    val roll: Float,
)