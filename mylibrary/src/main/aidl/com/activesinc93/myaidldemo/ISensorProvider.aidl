package com.activesinc93.myaidldemo;

interface ISensorProvider {
    int addNumbers(int num1, int num2);
    void getOrientationSensorData();
}